import os
import tkinter as tk
import customtkinter as ctk
from PIL import Image
import pyperclip
import mysql.connector as mysql
from mysql.connector import Error as DbError, MySQLConnection
from encryption import password_decrypt, password_encrypt
from cryptography import exceptions as CryptoErrors, fernet

def clipboard(value:str):
    pyperclip.copy(value)

def lock_unlock_mp():
    global _mp_locked
    if _mp_locked:
        _mp_locked = False
        mp_lock_btn.configure(image=mp_unlocked_image)
        mp_entry.configure(state=tk.NORMAL, text_color=_white)
    else:
        _mp_locked = True
        mp_lock_btn.configure(image=mp_locked_image)
        mp_entry.configure(state=tk.DISABLED, text_color=_gray)

def insert_info(text:str, color:str):
    info_textbox.configure(state=tk.NORMAL)
    info_textbox.delete(0.0, 'end')
    info_textbox.insert(0.0, text)
    info_textbox.configure(text_color=color)
    info_textbox.configure(state=tk.DISABLED)

def set_settings():
    global _settings
    if len(_settings) != 3:
        return

    db_host_entry.delete(0, 'end')
    db_host_entry.insert(0, _settings[0])
    db_database_entry.delete(0, 'end')
    db_database_entry.insert(0, _settings[1])
    db_password_entry.delete(0, 'end')
    db_password_entry.insert(0, _settings[2])

def update_settings():
    global _settings
    _settings = [db_host_entry.get(), db_database_entry.get(), db_password_entry.get()]

def write_settings():
    global _settingsFile, _settings
    _settingsFile.seek(0)
    _settingsFile.truncate()
    _settingsFile.writelines([_settings[0].strip(), '\n', _settings[1].strip(), '\n', _settings[2].strip()])

def get_db(connect: bool) -> MySQLConnection:
    global _mydb

    db_config = {
        'host':db_host_entry.get(),
        'user':'root',
        'password':db_password_entry.get(),
        'database':db_database_entry.get(),
        'connection_timeout':10,
        'port':3306
    }

    try:
        if not _mydb and connect:
            _mydb = mysql.connect(**db_config)
    except DbError as e:
        insert_info('Error connecting to db: {}'.format(e.msg), _red)
        _mydb = None
    finally:
        return _mydb

def connect_to_db():
    try: 
        db = get_db(True)
        if not db:
            return
        elif db.is_connected():
            insert_info('Successfully connected. Server info: {}.'.format(db.get_server_info()), _green)
            db_password_entry.configure(state=tk.DISABLED, text_color=_gray)
            db_host_entry.configure(state=tk.DISABLED, text_color=_gray)
            db_database_entry.configure(state=tk.DISABLED, text_color=_gray)
    except DbError as e:
        insert_info('DbError: {}'.format(e.msg), _red)
    finally:
        update_settings()
        write_settings()

def disconnect_db():
    global _mydb
    if not _mydb:
        insert_info('Already disconnected.', _white)
    else:
        _mydb.close()
        _mydb = None
        insert_info('Successfully disconnected.', _green)
    
    db_password_entry.configure(state=tk.NORMAL, text_color=_white)
    db_host_entry.configure(state=tk.NORMAL, text_color=_white)
    db_database_entry.configure(state=tk.NORMAL, text_color=_white)

def add(masterpw: str, account: str, username: str, password: str):
    db = get_db(False)
    if not db:
        insert_info('Missing Db connection.', _red)
        return

    if len(account) == 0:
        insert_info('Invalid account.', _red)
        return
    
    if _mp_locked == False:
        insert_info('Unlocked master password.', _red)
        return

    encrypted_pw = password_encrypt(password.encode(), masterpw)
    mycursor = db.cursor()

    try:
        sql = 'INSERT INTO passwords (account, password, username) VALUES (%s, %s, %s);'
        val = (account, encrypted_pw, username)
        mycursor.execute(sql, val)
        db.commit()

        insert_info('Successfully inserted {} records.'.format(mycursor.rowcount), _green)

        add_account_entry.delete(0, 'end')
        add_password_entry.delete(0, 'end')
        add_username_entry.delete(0, 'end')
    except DbError as e:
        insert_info('DbError: {}'.format(e.msg), _red)
    finally:
        mycursor.close()
        del encrypted_pw

def get(masterpw: str, account: str, selection: str):
    db = get_db(False)
    if not db:
        insert_info('Missing Db connection.', _red)
        return
    
    if len(account) == 0:
        insert_info('Invalid account.', _red)
        return
    
    if _mp_locked == False:
        insert_info('Unlocked master password.', _red)
        return

    decrypted_pw = None
    mycursor = db.cursor()

    try:
        sql = None
        if selection == 'username':
            sql = 'SELECT username FROM passwords WHERE account=%s;'
        elif selection == 'password':
            sql = 'SELECT password FROM passwords WHERE account=%s;'

        if not sql:
            insert_info('{} not found for account == {}.'.format(selection,account), _red)
            return

        val = (account,)
        mycursor.execute(sql, val)
        result = mycursor.fetchone()
        if not result:
            insert_info('{} not found for account == {}.'.format(selection,account), _red)
            return

        value = result[0]
        if selection == 'password':
            decrypted_pw = password_decrypt(value, masterpw).decode()
            clipboard(decrypted_pw)
            insert_info('Successfully copied password to clipboard.', _green)
        elif selection == 'username':
            if not value:
                insert_info('username not available for account == {}.'.format(account), _red)
            elif len(value) == 0:
                insert_info('username not available for account == {}.'.format(account), _red)
            else:
                clipboard(result[0])
                insert_info('Successfully copied username to clipboard.', _green)

    except DbError as e:
        insert_info('DbError: {}'.format(e.msg), _red)
    except CryptoErrors.InvalidSignature:
        insert_info('A cryptographic error occurred, probably caused by a wrong master password.', _red)
    except fernet.InvalidToken:
        insert_info('A cryptographic error occurred, probably caused by a wrong master password.', _red)
    finally:
        mycursor.close()
        del decrypted_pw

def delete(account:str):
    db = get_db(False)
    if not db:
        insert_info('Missing Db connection.', _red)
        return

    mycursor = db.cursor()
    try:
        sql = 'DELETE FROM passwords WHERE account=%s;'
        val = (account,)
        mycursor.execute(sql, val)
        db.commit()
        insert_info('Successfully deleted {} records.'.format(mycursor.rowcount), _green)
    except DbError as e:
        insert_info('DbError: {}'.format(e.msg), _red)
    finally:
        mycursor.close()

def fetchall() -> []:
    db = get_db(False)
    if not db:
        insert_info('Missing Db connection.', _red)
        return
    
    for widget in accounts_frame.winfo_children():
        widget.destroy()

    mycursor = db.cursor()

    try:
        sql = 'SELECT account FROM passwords ORDER BY account;'
        mycursor.execute(sql)
        result = mycursor.fetchall()
        insert_info('Successfully fetched {} records.'.format(mycursor.rowcount), _green)

        for i, record in enumerate(result):
            record_frame = ctk.CTkFrame(accounts_frame)
            record_frame.pack(fill=tk.X, pady=_small_pady)
            account_text = ctk.CTkLabel(record_frame, text=record[0], font=_font)
            account_text.pack()

            if i < len(result) - 1:
                line_separator = ctk.CTkFrame(record_frame, height=5, fg_color='gray10')
                line_separator.pack(fill=tk.X)


    except DbError as e:
        insert_info('DbError: {}'.format(e.msg), _red)
    finally:
        mycursor.close()

def update_username(account:str, username:str):
    db = get_db(False)
    if not db:
        insert_info('Missing Db connection.', _red)
        return

    mycursor = db.cursor()
    try:
        sql = 'UPDATE passwords SET username = %s WHERE account=%s;'
        val = (username, account)
        mycursor.execute(sql, val)
        db.commit()
        insert_info('Successfully updated {} records.'.format(mycursor.rowcount), _green)
    except DbError as e:
        insert_info('DbError: {}'.format(e.msg), _red)
    finally:
        mycursor.close()


# GUI
_green = 'green1'
_red = 'red1'
_white = 'white'
_gray = 'gray'
_transparent = 'transparent'

ctk.set_appearance_mode('System')
ctk.set_default_color_theme('green')

app = ctk.CTk()
app.geometry('1280x720')
app.title('R1DDL3')

_font = ctk.CTkFont('roboto', 16, 'bold', 'roman')
_mp_font = ctk.CTkFont('roboto', 16, 'bold', 'roman', underline=True)
_std_padx = 10
_small_pady = 5
_big_pady = 15

# Left frame
left_frame = ctk.CTkScrollableFrame(app, width=500)
left_frame.pack(side=tk.LEFT, fill=tk.Y, padx=_std_padx, pady=_small_pady)

# Master Password
mp_frame = ctk.CTkFrame(left_frame)
mp_label = ctk.CTkLabel(mp_frame, text='master password', font=_mp_font)
mp_label.pack(side=tk.LEFT, padx=_std_padx)

mp_entry = ctk.CTkEntry(mp_frame, width=200, show=u'\u25cf')
mp_entry.pack(side=tk.RIGHT, padx=_std_padx)

mp_locked_image = ctk.CTkImage(light_image=Image.open('images/closedlock.png'), dark_image=Image.open('images/closedlock.png'), size=(30,30))
mp_unlocked_image = ctk.CTkImage(light_image=Image.open('images/openlock.png'), dark_image=Image.open('images/openlock.png'), size=(30,30))

mp_lock_btn = ctk.CTkButton(mp_frame, width=30, height=30, command=lock_unlock_mp, image=mp_unlocked_image, text='', font=_font)
mp_lock_btn.pack(side=tk.RIGHT, padx=_std_padx)

mp_frame.pack(fill=tk.X, pady=_big_pady)

# DB IP
db_frame = ctk.CTkFrame(left_frame)
db_host_frame = ctk.CTkFrame(db_frame)
db_host_label = ctk.CTkLabel(db_host_frame, text='db host', font=_font)
db_host_label.pack(side=tk.LEFT, padx=_std_padx)
db_host_entry = ctk.CTkEntry(db_host_frame, width=200, font=_font)
db_host_entry.pack(side=tk.RIGHT, padx=_std_padx)
db_host_frame.pack(fill=tk.X, pady=_small_pady)

# DB database
db_database_frame = ctk.CTkFrame(db_frame)
db_database_label = ctk.CTkLabel(db_database_frame, text='db database name', font=_font)
db_database_label.pack(side=tk.LEFT, padx=_std_padx, pady=_small_pady)
db_database_entry = ctk.CTkEntry(db_database_frame, width=200, font=_font)
db_database_entry.pack(side=tk.RIGHT, padx=_std_padx)
db_database_frame.pack(fill=tk.X, pady=_small_pady)

# DB password
db_password_frame = ctk.CTkFrame(db_frame)
db_password_label = ctk.CTkLabel(db_password_frame, text='db password', font=_font)
db_password_label.pack(side=tk.LEFT, padx=_std_padx, pady=_small_pady)
db_password_entry = ctk.CTkEntry(db_password_frame, width=200, show=u'\u25cf')
db_password_entry.pack(side=tk.RIGHT, padx=_std_padx)
db_password_frame.pack(fill=tk.X, pady=_small_pady)

# Connect to DB
connect_btn = ctk.CTkButton(db_frame, width=200, text='CONNECT TO DB', command=lambda: connect_to_db(), font=_font)
connect_btn.pack(pady=_small_pady)

# Disconnect from DB
disconnect_btn = ctk.CTkButton(db_frame, width=200, text='DISCONNECT FROM DB', command=disconnect_db, font=_font, fg_color=_red, hover_color='darkred')
disconnect_btn.pack(pady=_small_pady)

db_frame.pack(fill=tk.X, pady=_big_pady)


# Add account/password
add_frame = ctk.CTkFrame(left_frame)

add_account_frame = ctk.CTkFrame(add_frame);
add_account_label = ctk.CTkLabel(add_account_frame, text='add account', font=_font)
add_account_label.pack(side=tk.LEFT, padx=_std_padx)
add_account_entry = ctk.CTkEntry(add_account_frame, width=200, font=_font)
add_account_entry.pack(side=tk.RIGHT, padx=_std_padx)
add_account_frame.pack(fill=tk.X, pady=_small_pady)

add_username_frame = ctk.CTkFrame(add_frame);
add_username_label = ctk.CTkLabel(add_username_frame, text='add username', font=_font)
add_username_label.pack(side=tk.LEFT, padx=_std_padx)
add_username_entry = ctk.CTkEntry(add_username_frame, width=200, font=_font)
add_username_entry.pack(side=tk.RIGHT, padx=_std_padx)
add_username_frame.pack(fill=tk.X, pady=_small_pady)

add_password_frame = ctk.CTkFrame(add_frame);
add_password_label = ctk.CTkLabel(add_password_frame, text='add password', font=_font)
add_password_label.pack(side=tk.LEFT, padx=_std_padx)
add_password_entry = ctk.CTkEntry(add_password_frame, width=200, show=u'\u25cf')
add_password_entry.pack(side=tk.RIGHT, padx=_std_padx)
add_password_frame.pack(fill=tk.X, pady=_small_pady)

add_btn = ctk.CTkButton(add_frame, width=200, text='ADD', command=lambda: add(mp_entry.get(),
                                                                                    add_account_entry.get(),
                                                                                    add_username_entry.get(),
                                                                                    add_password_entry.get()),
                                                                                    font=_font)
add_btn.pack(pady=_small_pady)

add_frame.pack(fill=tk.X, pady=_big_pady)

# Get password/username for account
get_frame = ctk.CTkFrame(left_frame)

get_account_frame = ctk.CTkFrame(get_frame)
get_account_label = ctk.CTkLabel(get_account_frame, text='get account', font=_font)
get_account_label.pack(side=tk.LEFT, padx=_std_padx, pady=_small_pady)
get_account_entry = ctk.CTkEntry(get_account_frame, width=200, font=_font)
get_account_entry.pack(side=tk.RIGHT, padx=_std_padx)
get_account_frame.pack(fill=tk.X, pady=_small_pady)

get_password_btn = ctk.CTkButton(get_frame, width=200, text='GET PASSWORD', command=lambda: get(mp_entry.get(), get_account_entry.get(), 'password'), font=_font)
get_password_btn.pack(pady=_small_pady)

get_username_btn = ctk.CTkButton(get_frame, width=200, text='GET USERNAME', command=lambda: get(mp_entry.get(), get_account_entry.get(), 'username'), font=_font)
get_username_btn.pack(pady=_small_pady)

get_frame.pack(fill=tk.X, pady=_big_pady)

# Edit username
edit_frame = ctk.CTkFrame(left_frame)
edit_account_frame = ctk.CTkFrame(edit_frame)
edit_account_label = ctk.CTkLabel(edit_account_frame, text='edit account', font=_font)
edit_account_label.pack(side=tk.LEFT, padx=_std_padx, pady=_small_pady)
edit_account_entry = ctk.CTkEntry(edit_account_frame, width=200, font=_font)
edit_account_entry.pack(side=tk.RIGHT, padx=_std_padx)
edit_account_frame.pack(fill=tk.X, pady=_small_pady)

edit_username_frame = ctk.CTkFrame(edit_frame)
edit_username_label = ctk.CTkLabel(edit_username_frame, text='edit username', font=_font)
edit_username_label.pack(side=tk.LEFT, padx=_std_padx, pady=_small_pady)
edit_username_entry = ctk.CTkEntry(edit_username_frame, width=200, font=_font)
edit_username_entry.pack(side=tk.RIGHT, padx=_std_padx)
edit_username_frame.pack(fill=tk.X, pady=_small_pady)


edit_account_btn = ctk.CTkButton(edit_frame, width=200, text='EDIT', command=lambda: update_username(edit_account_entry.get(), edit_username_entry.get()), 
                                font=_font)
edit_account_btn.pack(pady=_small_pady)
edit_frame.pack(fill=tk.X, pady=_big_pady)


# Delete account/password
del_frame = ctk.CTkFrame(left_frame)
del_account_frame = ctk.CTkFrame(del_frame)
del_account_label = ctk.CTkLabel(del_account_frame, text='delete account', font=_font)
del_account_label.pack(side=tk.LEFT, padx=_std_padx, pady=_small_pady)
del_account_entry = ctk.CTkEntry(del_account_frame, width=200, font=_font)
del_account_entry.pack(side=tk.RIGHT, padx=_std_padx)
del_account_frame.pack(fill=tk.X, pady=_small_pady)

del_account_btn = ctk.CTkButton(del_frame, width=200, text='DELETE', command=lambda: delete(del_account_entry.get()), 
                                fg_color=_red, hover_color='darkred', font=_font)
del_account_btn.pack(pady=_small_pady)
del_frame.pack(fill=tk.X, pady=_big_pady)

# Account list
fetch_accounts_btn = ctk.CTkButton(app, width=200, text='FETCH ALL ACCOUNTS', font=_font, command=fetchall)
fetch_accounts_btn.pack(padx=_std_padx, pady=_small_pady)

accounts_frame = ctk.CTkScrollableFrame(app, label_text='accounts', label_font=_mp_font)
accounts_frame.pack(fill=tk.BOTH, expand=True, padx=_std_padx, pady=_small_pady)

# Info text
info_frame = ctk.CTkFrame(app)
info_label = ctk.CTkLabel(info_frame, text='info', font=_font)
info_label.pack(side=tk.LEFT, padx=_std_padx, pady=_small_pady, anchor=tk.NW)
info_textbox = ctk.CTkTextbox(info_frame, font=_font, state=tk.DISABLED, wrap=tk.WORD, height=100)
info_textbox.pack(side=tk.RIGHT, fill=tk.BOTH, expand=True, padx=_std_padx, pady=_small_pady)
info_frame.pack(fill=tk.X, padx=_std_padx, pady=_big_pady)


# AppIcon
app.wm_iconbitmap('app.ico')

# Init
_mydb = None
_mp_locked = False

_settings = []
_settingsPath = 'settings.txt'
_settingsFile = None
if (os.path.exists(_settingsPath)):
    _settingsFile = open(_settingsPath, 'r+')
    _settings = _settingsFile.readlines()
    set_settings()
else:
    _settingsFile = open(_settingsPath, 'w+')

# Run app
app.mainloop()